# Welcome

This is the wiki of Modern Application Development Course.

???+ note

    This is a wiki.  It's a live document and subject to continuous updates. 


## How to contribute
We use [mkdocs.org](https://www.mkdocs.org) and [Material for MkDocs ](https://squidfunk.github.io/mkdocs-material/). You can check the respective site for more information

### Setup locally
* Install mkdocs
* Fork and Clone the git project
* Go to the folder
* Run `mkdocs serve` to start the live-reloading docs server
* Open [http://localhost:8000](http://localhost:8000) to see the wiki locally
* Go to `docs/` folder and edit the markdown content file or add more content
* Check on the localhost url
* If it looks okay. Then commit and send a merge request to be included

## Contact
* Contact 